/*
 * Error codes
 *
 *  Created on: Jan 2, 2021
 *      Author: bastian
 */

#ifndef ERROR_H_
#define ERROR_H_

#include <stdint.h>

typedef enum
{
	PM_OK = 0U,
	PM_FAIL,
	DELEAYED,
	OUT_OF_BOUNDS,
	UNAVAILABLE,
	ABORT,

} error_code_t;

#if 0
#define bool uint8_t
#define true 1
#define false 0
#endif

#endif /* ERROR_H_ */
