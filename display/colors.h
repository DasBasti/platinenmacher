/*
 * colors.h
 *
 *  Created on: Jan 2, 2021
 *      Author: bastian
 */

#ifndef PLATINENMACHER_DISPLAY_COLORS_H_
#define PLATINENMACHER_DISPLAY_COLORS_H_

/**********************************
 * Available Colors on ACeP 5.65"
 *********************************/
typedef enum
{
#ifdef EINK_7COLOR
	BLACK,		 /// 000
	WHITE,		 ///	010
	GREEN,		 ///	011
	BLUE,		 ///	001
	RED,		 ///	100
	YELLOW,		 ///	101
	ORANGE,		 ///	110
	TRANSPARENT, ///	111   unavailable  Afterimage
#elif EINK_3COLOR
	WHITE,		 /// 00
	BLACK,		 /// 01
	RED,		 /// 10
	TRANSPARENT, /// 11
#else
	BLACK,
	WHITE,
	TRANSPARENT,
#endif
} color_t;

#endif /* PLATINENMACHER_DISPLAY_COLORS_H_ */
