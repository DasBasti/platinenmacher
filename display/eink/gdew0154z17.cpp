/*
 * gdew0154z17.c
 *
 *  Created on: Mar 3, 2021
 *      Author: bastian
 */
#include <string.h>

extern "C"
{
#include "gdew0154z17.h"
#include <hw/gpio.h>
#include <display/display.h>
#include <display/colors.h>
}

#include <Arduino.h>
#include <SPI.h>

// The framebuffer for the display
#define FB_SIZE (GDEW0154Z17_WIDTH * GDEW0154Z17_HEIGHT)
uint8_t fb[FB_SIZE] = {};
// SPI handle
gdew0154z17_io_config_t *io;

static error_code_t
_lcd_chkstatus()
{
    uint8_t wait = 0;
    delay(100);
    while (!(gpio_read(io->busy)))
    {
        if (wait++ >= 100)
            return PM_FAIL;
        //Serial.print(".");
        delay(500);
    }
    //Serial.println("");
    //Serial.println("Wait done");
    return PM_OK;
}

static void GDEW0154Z17_SendCommand(uint8_t Reg)
{
    gpio_write(io->dc, GPIO_RESET);
    gpio_write(io->cs, GPIO_RESET);
    SPI.transfer(Reg);
    //Serial.print("CMD: 0x");
    //Serial.println(Reg, HEX);
    gpio_write(io->cs, GPIO_SET);
}

static void GDEW0154Z17_SendData(uint8_t Data)
{
    gpio_write(io->dc, GPIO_SET);
    gpio_write(io->cs, GPIO_RESET);
    SPI.transfer(Data);
    //Serial.print("DAT: 0x");
    //Serial.println(Data, HEX);
    gpio_write(io->cs, GPIO_SET);
}

static error_code_t _init()
{
    uint8_t DISPLAY_HRES = 0x98;       // 152 resolution
    uint8_t DISPLAY_VRES_byte1 = 0x00; // 152
    uint8_t DISPLAY_VRES_byte2 = 0x98;

    GDEW0154Z17_SendCommand(IL0373_BOOSTER_SOFT_START); // booster soft start
    GDEW0154Z17_SendData(0x17);                         // A
    GDEW0154Z17_SendData(0x17);                         // B
    GDEW0154Z17_SendData(0x17);                         // C

    GDEW0154Z17_SendCommand(IL0373_POWER_SETTING); // power settings
    GDEW0154Z17_SendData(0x03);                    // VDS_EN, VDG_EN
    GDEW0154Z17_SendData(0x03);                    // VCOM_HV, VGHL_LV[1], VGHL_LV[0] +-13V
    GDEW0154Z17_SendData(0x2b);                    // VDH
    GDEW0154Z17_SendData(0x2b);                    // VDL
    GDEW0154Z17_SendData(0x09);                    // VDHR

    GDEW0154Z17_SendCommand(IL0373_POWER_ON);         // power on
    GDEW0154Z17_SendCommand(IL0373_POWER_ON_MEASURE); // power measure
    *(io->adc_val) = analogReadMilliVolts(io->adc);

    if (PM_OK != _lcd_chkstatus())
        return PM_FAIL;

    GDEW0154Z17_SendCommand(IL0373_PANEL_SETTING); // panel setting
    GDEW0154Z17_SendData(0x0f);
    //GDEW0154Z17_SendData(0xcf);                    // LUT from OTP
    //GDEW0154Z17_SendData(0xef); // LUT from register 160x296
    // GDEW0154Z17_SendData(0x0d); //VCOM to 0V fast

    GDEW0154Z17_SendCommand(IL0373_PLL);
    //GDEW0154Z17_SendData(0x3a); // 3A 100Hz   29 150Hz   39 200Hz    31 171Hz
    // 3C 50Hz (default)    0B 10Hz

    GDEW0154Z17_SendCommand(IL0373_RESOLUTION); // resolution setting
    GDEW0154Z17_SendData(DISPLAY_HRES);
    GDEW0154Z17_SendData(DISPLAY_VRES_byte1);
    GDEW0154Z17_SendData(DISPLAY_VRES_byte2);

    GDEW0154Z17_SendCommand(IL0373_CDI); // VCM_DC_SETTING
    GDEW0154Z17_SendData(0x87);

    //GDEW0154Z17_SendCommand(IL0373_VCM_DC_SETTING); // VCM_DC_SETTING
    // GDEW0154Z17_SendData(0x0e);
    //GDEW0154Z17_SendData(0x12);

    //GDEW0154Z17_SendCommand(IL0373_CDI); // VCOM AND DATA INTERVAL SETTING
    //  GDEW0154Z17_SendData(0x77); // WBmode:VBDF 17|D7 VBDW 97 VBDB 57
    //GDEW0154Z17_SendData(0x37);     // WBRmode:VBDF F7 VBDW 77 VBDB 37  VBDR B7
    //GDEW0154Z17_SendData(0x87);
    //Serial.println("Display OK");
    return PM_OK;
}

static void GDEW0154Z17_Display(uint8_t *image)
{
    // write black
    uint32_t size = FB_SIZE;

    /*   {[00][00][10][01][00][00][10][01]}
          [00100010] ->sw
          [00010001] ->rot
    */
    // write black
    GDEW0154Z17_SendCommand(0x10);
    for (uint32_t p = 0; p < size; p += 8)
    {
        uint8_t pixel = 0;
        for (uint8_t i = 0; i < 8; i++)
        {
            pixel += (fb[p + i] & BLACK) << (7 - i);
        }
        GDEW0154Z17_SendData(pixel);
    }

    // write red
    GDEW0154Z17_SendCommand(0x13);
    for (uint32_t p = 0; p < size; p += 8)
    {
        uint8_t pixel = 0;
        for (uint8_t i = 0; i < 8; i++)
        {
            if (fb[p + i] & RED)
                pixel += 1 << (7 - i);
        }
        GDEW0154Z17_SendData(pixel);
    }

    GDEW0154Z17_SendCommand(0x12);
    _lcd_chkstatus();
    GDEW0154Z17_SendCommand(IL0373_CDI);
    GDEW0154Z17_SendData(0x07); // floating
    GDEW0154Z17_SendCommand(IL0373_POWER_OFF);
    GDEW0154Z17_SendCommand(IL0373_DEEP_SLEEP);
    GDEW0154Z17_SendData(0xa5); // sleep
}

static void GDEW0154Z17_Commit_Fb()
{
    GDEW0154Z17_Display(fb);
}

error_code_t GDEW0154Z17_Write(display_t *dsp, uint16_t x, uint16_t y, uint8_t color)
{
    uint32_t position;
    //Serial.println( "pixel %d/%d", x, y);
    switch (dsp->rotation)
    {
    case DISPLAY_ROTATE_270: // switch x and y and invert
        position = (((GDEW0154Z17_HEIGHT - 1 - x) * GDEW0154Z17_WIDTH) + y);
        break;
    case DISPLAY_ROTATE_90: // switch x and y and invert
        position = ((x * GDEW0154Z17_WIDTH) + GDEW0154Z17_HEIGHT - y);
        break;
    default: // default is rotate 0 and no change
        position = ((y * GDEW0154Z17_WIDTH) + x);
    }
    if (position < FB_SIZE)
    {
        // position in fb is calculated by position and offset in byte
        fb[position] = color;
        return PM_OK;
    }
    return OUT_OF_BOUNDS;
}

uint8_t GDEW0154Z17_Decompress_Pixel(rect_t *size, uint16_t x, uint16_t y, uint8_t *data)
{

    uint32_t pos = (y * size->height) + x;
    uint8_t offset = pos % 8;

    return (data[(pos / 8)] & (1 << (7 - offset)));
}

display_t *GDEW0154Z17_Init(display_rotation_t rotation, gdew0154z17_io_config_t *io_config)
{
    io = io_config;

    SPI.setFrequency(1000000);
    SPI.begin(io->sclk, -1, io->mosi, -1);

    /* create display object */
    display_t *disp;
    switch (rotation)
    {
    case DISPLAY_ROTATE_90:
    case DISPLAY_ROTATE_270:
        disp = display_init(GDEW0154Z17_HEIGHT, GDEW0154Z17_WIDTH, 4, rotation);
        break;
    default:
        disp = display_init(GDEW0154Z17_WIDTH, GDEW0154Z17_HEIGHT, 4, rotation);
        break;
    }
    /* assign driver functions */
    disp->update = GDEW0154Z17_Commit_Fb;
    disp->write_pixel = GDEW0154Z17_Write;
    disp->decompress = GDEW0154Z17_Decompress_Pixel;

    /*    io->dc->pp = PAD_PULLUP;
    gpio_update_mode(io->dc);
    Serial.print("DC: ");
    Serial.println(io->dc->pin);

    io->busy->pp = PAD_PULLUP;
    gpio_update_mode(io->busy);
    Serial.print("DC: ");
    Serial.println(io->dc->pin);

    io->res->pp = PAD_PULLUP;
    gpio_update_mode(io->res);
    Serial.print("DC: ");
    Serial.println(io->dc->pin);

    io->cs->pp = PAD_PULLUP;
    gpio_update_mode(io->res);
  */
    gpio_write(io->dc, GPIO_RESET);
    gpio_write(io->cs, GPIO_RESET);

    gpio_write(io->res, GPIO_SET);
    delay(100);
    gpio_write(io->res, GPIO_RESET);
    delay(100);
    gpio_write(io->res, GPIO_SET);
    delay(100);
    gpio_write(io->cs, GPIO_SET);

    memset(fb, 0, sizeof(fb));

    if (PM_OK != _init())
        disp = NULL;

    return disp;
}
