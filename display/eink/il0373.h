/**
 * SchmartWatsch Firmware - Driver for IL0373 EPD Controler
 * 
 * Author: Bastian Neumann 2018
 * 
 */

#ifndef IL0373_H
#define IL0373_H
#include <stdint.h>

typedef enum
{
    RES_96x230,
    RES_96x252,
    RES_128x296,
    RES_160x296
} il0373_resolution_t;

typedef enum
{
    DIR_UP,
    DIR_DOWN,
    DIR_LEFT,
    DIR_RIGHT
} il0373_direction_t;

#define VDS_EN 1
#define VDG_EN 2
#define VCOM_HV 4
#define VGHL_LV16 0
#define VGHL_LV15 1
#define VGHL_LV14 2
#define VGHL_LV13 3

#define IL0373_PANEL_SETTING 0x00
#define IL0373_POWER_SETTING 0x01
#define IL0373_POWER_OFF 0x02
#define IL0373_POWER_OFF_SEQUENCE 0x03
#define IL0373_POWER_ON 0x04
#define IL0373_POWER_ON_MEASURE 0x05
#define IL0373_BOOSTER_SOFT_START 0x06
#define IL0373_DEEP_SLEEP 0x07
#define IL0373_DTM1 0x10
#define IL0373_DATA_STOP 0x11
#define IL0373_DISPLAY_REFRESH 0x12
#define IL0373_PDTM1 0x14
#define IL0373_PDTM2 0x15
#define IL0373_PDRF 0x16
#define IL0373_LUT1 0x20
#define IL0373_LUTWW 0x21
#define IL0373_LUTBW 0x22
#define IL0373_LUTWB 0x23
#define IL0373_LUTBB 0x24
#define IL0373_LUT2 0x25
#define IL0373_LUTRED0 0x26
#define IL0373_LUTRED1 0x27
#define IL0373_PLL 0x30
#define IL0373_CDI 0x50
#define IL0373_RESOLUTION 0x61
#define IL0373_VCM_DC_SETTING 0x82

typedef enum
{
    VDH_2V4 = 0,
    VDH_2V6,
    VDH_2V8,
    VDH_3V0,
    VDH_3V2,
    VDH_3V4,
    VDH_3V6,
    VDH_3V8,
    VDH_4V0,
    VDH_4V2,
    VDH_4V4,
    VDH_4V6,
    VDH_4V8,
    VDH_5V0,
    VDH_5V2,
    VDH_5V4,
    VDH_5V6,
    VDH_5V8,
    VDH_6V0,
    VDH_6V2,
    VDH_6V4,
    VDH_6V6,
    VDH_6V8,
    VDH_7V0,
    VDH_7V2,
    VDH_7V4,
    VDH_7V6,
    VDH_7V8,
    VDH_8V0,
    VDH_8V2,
    VDH_8V4,
    VDH_8V6,
    VDH_8V8,
    VDH_9V0,
    VDH_9V2,
    VDH_9V4,
    VDH_9V6,
    VDH_9V8,
    VDH_10V0,
    VDH_10V2,
    VDH_10V4,
    VDH_10V6,
    VDH_10V8,
    VDH_11V0
} il0373_vdh_t;

typedef enum
{
    VDL_2V4 = 0,
    VDL_2V6,
    VDL_2V8,
    VDL_3V0,
    VDL_3V2,
    VDL_3V4,
    VDL_3V6,
    VDL_3V8,
    VDL_4V0,
    VDL_4V2,
    VDL_4V4,
    VDL_4V6,
    VDL_4V8,
    VDL_5V0,
    VDL_5V2,
    VDL_5V4,
    VDL_5V6,
    VDL_5V8,
    VDL_6V0,
    VDL_6V2,
    VDL_6V4,
    VDL_6V6,
    VDL_6V8,
    VDL_7V0,
    VDL_7V2,
    VDL_7V4,
    VDL_7V6,
    VDL_7V8,
    VDL_8V0,
    VDL_8V2,
    VDL_8V4,
    VDL_8V6,
    VDL_8V8,
    VDL_9V0,
    VDL_9V2,
    VDL_9V4,
    VDL_9V6,
    VDL_9V8,
    VDL_10V0,
    VDL_10V2,
    VDL_10V4,
    VDL_10V6,
    VDL_10V8,
    VDL_11V0
} il0373_vdl_t;

typedef enum
{
    VDHR_2V4 = 0,
    VDHR_2V6,
    VDHR_2V8,
    VDHR_3V0,
    VDHR_3V2,
    VDHR_3V4,
    VDHR_3V6,
    VDHR_3V8,
    VDHR_4V0,
    VDHR_4V2,
    VDHR_4V4,
    VDHR_4V6,
    VDHR_4V8,
    VDHR_5V0,
    VDHR_5V2,
    VDHR_5V4,
    VDHR_5V6,
    VDHR_5V8,
    VDHR_6V0,
    VDHR_6V2,
    VDHR_6V4,
    VDHR_6V6,
    VDHR_6V8,
    VDHR_7V0,
    VDHR_7V2,
    VDHR_7V4,
    VDHR_7V6,
    VDHR_7V8,
    VDHR_8V0,
    VDHR_8V2,
    VDHR_8V4,
    VDHR_8V6,
    VDHR_8V8,
    VDHR_9V0,
    VDHR_9V2,
    VDHR_9V4,
    VDHR_9V6,
    VDHR_9V8,
    VDHR_10V0,
    VDHR_10V2,
    VDHR_10V4,
    VDHR_10V6,
    VDHR_10V8,
    VDHR_11V0
} il0373_vdhr_t;

typedef struct
{
    uint8_t enable;
    uint8_t vcom;
    uint8_t vdh;
    uint8_t vdl;
    uint8_t vdhr;
} il0373_power_t;

typedef struct
{
    uint8_t btpha;
    uint8_t btphb;
    uint8_t btphc;
} il0373_booster_softstart_phase_t;

void il0373_spi_init();
void il0373_spi_start();
void il0373_spi_stop();
void il0373_spi_data_write(unsigned char data);
void il0373_spi_cmd_write(unsigned char command);
void il0373_spi_cmd_write_p1(unsigned char command,
                             unsigned char p1);
void il0373_spi_cmd_write_p2(unsigned char command,
                             unsigned char p1, unsigned char p2);
void il0373_spi_write(unsigned char *value,
                      unsigned char datalen);
void il0373_spi_write_frame(unsigned char XSize,
                            unsigned int YSize, unsigned char *buff);
unsigned char il0373_spi_data_read();
void il0373_set_resolution(il0373_resolution_t resolution);
void il0373_set_LUT_from_OTP(); // Set REG_EN flag
void il0373_set_LUT_from_register();
void il0373_set_BWR(); // Set BWR flag
void il0373_set_BW();
void il0373_set_scan_direction(il0373_direction_t direction);
void il0373_set_source_shift_direction(il0373_direction_t direction);
void il0373_set_booster(uint8_t on);
void il0373_reset();
void il0373_set_power(il0373_power_t pwr);
void il0373_power_off();
void il0373_power_on();
void il0373_set_power_off_sequence(uint8_t frames);
void il0373_set_power_measure_on();
void il0373_set_soft_start(il0373_booster_softstart_phase_t bsph);
void il0373_refresh();
void il0373_build_LUT_VCOM();
void il0373_build_LUT_W2W();
void il0373_build_LUT_B2W();
void il0373_build_LUT_W2B();
void il0373_build_LUT_B2B();
void il0373_set_pll(uint8_t m, uint8_t n);
void il0373_set_temp_calib(uint16_t calib);
void il0373_tempsens_int(uint8_t offset);
void il0373_tempsens_ext(uint8_t offset);
uint16_t il0373_temsens_read();
uint8_t il0373_read_lowpower();
uint8_t il0373_read_status();
void il0373_auto_measure_vcom(uint8_t time, uint8_t xon, uint8_t amvs, uint8_t amv, uint8_t amve);
uint8_t il0373_read_vcom();
void il0373_set_vcm(uint8_t vcom);
void il0373_start_partial();
void il0373_stop_partial();
uint16_t il0373_set_partial_window(uint8_t x, uint8_t y, uint8_t xe, uint8_t ye);

void il0373_programming_start();
void il0373_programming_stop();
void il0373_programming_read(uint8_t *result);

void il0373_power_saving(uint8_t vcomw, uint8_t sdw);

void il0373_activate_lut(uint8_t lut_name, const uint8_t *lut_data, uint8_t len);

/*extern uint8_t lut_vcom0[];
extern uint8_t lut_ww[];
extern uint8_t lut_bw[];
extern uint8_t lut_bb[];
extern uint8_t lut_wb[];

extern uint8_t lut_vcom0_quick[];
extern uint8_t lut_ww_quick[];
extern uint8_t lut_bw_quick[];
extern uint8_t lut_bb_quick[];
extern uint8_t lut_wb_quick[];
*/
#endif // IL0373_H