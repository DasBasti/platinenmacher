/*
 * GDEW015417Z.h
 *
 *  Created on: Mar, 3, 2021
 *      Author: bastian
 */
#ifndef __EPD_GDEW015417Z_H__
#define __EPD_GDEW015417Z_H__

#include "display/display.h"
#include "hw/gpio.h"
#include "error.h"

// TODO: SPI transaktion kapseln!
//#include <driver/spi_master.h>

#include "il0373.h"

#define GDEW0154Z17_WIDTH 152
#define GDEW0154Z17_HEIGHT 152

typedef struct
{
    gpio_t *busy;      // device busy pin
    gpio_t *res;       // reset device
    gpio_t *dc;        // data or command
    gpio_t *cs;        // spi chip select
    uint8_t sclk;      // spi clock
    uint8_t mosi;      // spi data in
    uint8_t adc;       // adc pin
    uint16_t *adc_val; // value of last adc reading
} gdew0154z17_io_config_t;

display_t *GDEW0154Z17_Init(display_rotation_t rotation, gdew0154z17_io_config_t *io);
error_code_t GDEW0154Z17_Write(display_t *dsp, uint16_t x, uint16_t y, uint8_t color);
uint8_t GDEW0154Z17_Decompress_Pixel(rect_t *size, uint16_t x, uint16_t y, uint8_t *data);

#endif /*__EPD_GDEW015417Z_H__*/