/*
 * ESP32 implementation for handling GPIO
 *
 *  Created on: Jan 6, 2021
 *      Author: bastian
 */

#include "hw/gpio.h"
#include "error.h"
#include "memory.h"

#include <Arduino.h>

gpio_t *gpio_create(gpio_direction_t direction, uint32_t pin)
{
	gpio_t *gpio = RTOS_Malloc(sizeof(gpio_t));
	gpio->direction = direction;
	gpio->port = NULL;
	gpio->pin = pin;

	if (gpio->direction == PAD_INPUT)
		pinMode(gpio->pin, INPUT);
	else if (gpio->direction == PAD_OUTPUT)
		pinMode(gpio->pin, OUTPUT);
	else if (gpio->direction == PAD_ANALOG_IN)
		return gpio;
	else if (gpio->direction == PAD_ANALOG_OUT)
		return gpio;
	else if (gpio->direction == PAD_DISABLED)
		pinMode(gpio->pin, INPUT);

	return gpio;
}
void gpio_deinit(gpio_t *gpio)
{
}

void gpio_update_mode(gpio_t *gpio)
{
	pinMode(gpio->pin, gpio->pp);
}

void gpio_write(gpio_t *gpio, gpio_value_t value)
{
	//if (gpio->direction == PAD_OUTPUT)
	{
		digitalWrite(gpio->pin, value);
	}
}

gpio_value_t gpio_read(gpio_t *gpio)
{
	return digitalRead(gpio->pin);
}
