/**
 * SchmartWatsch Firmware - M41T62LC6F RTC Driver
 *
 * Author: Bastian Neumann 2018
 *
 */

#include "m41t62lc6f.h"
#include <string.h>

static void (*alarm_callback)(void);

typedef struct
{
  // 0x00
  uint8_t hundredthseconds : 4;
  uint8_t tenthseconds : 4;
  // 0x01
  uint8_t seconds : 4;
  uint8_t tenseconds : 3;
  uint8_t ST : 1;
  // 0x02
  uint8_t minutes : 4;
  uint8_t tenminutes : 3;
  uint8_t OFIE : 1;
  // 0x03
  uint8_t hours : 4;
  uint8_t tenhours : 2;
  uint8_t zero1 : 2;
  // 0x04
  uint8_t dayofweek : 3;
  uint8_t zero2 : 1;
  uint8_t RS : 4;
  // 0x05
  uint8_t date : 4;
  uint8_t tendate : 2;
  uint8_t zero3 : 2;
  // 0x06
  uint8_t month : 4;
  uint8_t tenmonth : 1;
  uint8_t zero4 : 1;
  uint8_t CB10 : 2;
  // 0x07
  uint8_t year : 4;
  uint8_t tenyear : 4;
  // 0x08
  uint8_t calibration : 5;
  uint8_t S : 1;
  uint8_t zero5 : 2;
  // 0x09
  uint8_t RB10 : 2;
  uint8_t BMB : 5;
  uint8_t RB2 : 1;
  // 0x0a
  uint8_t alarmmonth : 4;
  uint8_t alarmtenmonth : 1;
  uint8_t zero6 : 1;
  uint8_t SQWE : 1;
  uint8_t AFE : 1;
  // 0x0b
  uint8_t alarmdate : 4;
  uint8_t alarmtendate : 2;
  uint8_t RPT5 : 1;
  uint8_t RPT4 : 1;
  // 0x0c
  uint8_t alarmhour : 4;
  uint8_t alarmtenhour : 2;
  uint8_t zero7 : 1;
  uint8_t RPT3 : 1;
  // 0x0d
  uint8_t alarmminutes : 4;
  uint8_t alarmtenminutes : 3;
  uint8_t RPT2 : 1;
  // 0x0e
  uint8_t alarmseconds : 4;
  uint8_t alarmtenseconds : 3;
  uint8_t RPT1 : 1;
  // 0x0f
  uint8_t zero9 : 2;
  uint8_t OF : 1;
  uint8_t zero8 : 3;
  uint8_t AF : 1;
  uint8_t WDF : 1;
} rtc_data_t;

typedef union
{
  rtc_data_t fields;
  uint8_t bytes[16];
} rtc_memory_u;

static rtc_memory_u rtc_memory;

static inline uint8_t read_single_register(uint8_t addr)
{
  uint8_t data = 0;
  error_code_t err_code;
  //twi_tx_done = false;
  //err_code = nrf_drv_twi_tx(&m_twi, M41T62_I2C_ADDR, &addr, 1, 0);
  //while (!twi_tx_done)
  //{
  //  __WFE();
  //}
  //APP_ERROR_CHECK(err_code);
  //twi_rx_done = false;
  //err_code = nrf_drv_twi_rx(&m_twi, M41T62_I2C_ADDR, &data, sizeof(data));
  //while (!twi_rx_done)
  //{
  //  __WFE();
  //}
  //APP_ERROR_CHECK(err_code);
  return data;
}

static inline uint8_t write_single_register(uint8_t addr, uint8_t data)
{
  uint8_t payload[] = {addr, data};
  error_code_t err_code;
  /*twi_tx_done = false;
  err_code =
      nrf_drv_twi_tx(&m_twi, M41T62_I2C_ADDR, payload, sizeof(payload), 0);
  while (!twi_tx_done)
  {
    __WFE();
  }
  APP_ERROR_CHECK(err_code);
  */
  return payload[1];
}

static inline void read_whole_memory()
{
  uint8_t addr = 0;
  /*ret_code_t err_code;
  twi_tx_done = false;
  err_code = nrf_drv_twi_tx(&m_twi, M41T62_I2C_ADDR, &addr, 1, 0);
  while (!twi_tx_done)
  {
    __WFE();
  }
  APP_ERROR_CHECK(err_code);
  twi_rx_done = false;
  err_code = nrf_drv_twi_rx(&m_twi, M41T62_I2C_ADDR, (uint8_t *)&rtc_memory,
                            sizeof(rtc_memory));
  while (!twi_rx_done)
  {
    __WFE();
  }
  APP_ERROR_CHECK(err_code);
  */
  read_single_register(0); // jump to first register
}

static inline void write_whole_memory()
{
  struct
  {
    uint8_t addr;
    rtc_data_t mem;
  } payload;
  payload.addr = 0;
  memcpy(&payload.mem, &rtc_memory, sizeof(rtc_memory));

  /*
  ret_code_t err_code;
  twi_tx_done = false;
  err_code = nrf_drv_twi_tx(&m_twi, M41T62_I2C_ADDR, (uint8_t *)&payload,
                            sizeof(payload), 0);
  while (!twi_tx_done)
  {
    __WFE();
  }
  APP_ERROR_CHECK(err_code);
  twi_rx_done = false;
  err_code = nrf_drv_twi_rx(&m_twi, M41T62_I2C_ADDR, (uint8_t *)&rtc_memory,
                            sizeof(rtc_memory));
  while (!twi_rx_done)
  {
    __WFE();
  }
  APP_ERROR_CHECK(err_code);
  */
  read_single_register(0); // jump to first register
}
uint32_t m41t62_milliseconds()
{
  uint8_t raw = read_single_register(0x00);
  rtc_memory.fields.hundredthseconds = (raw & 0xf);
  rtc_memory.fields.tenthseconds = (raw & 0xf0 >> 4);
  return (rtc_memory.fields.tenthseconds * 10) +
         rtc_memory.fields.hundredthseconds;
}
uint8_t m41t62_seconds()
{
  uint8_t raw = read_single_register(0x01);
  rtc_memory.fields.seconds = (raw & 0xf);
  rtc_memory.fields.tenseconds = ((raw & 0x70) >> 4);
  return (rtc_memory.fields.tenseconds * 10) + rtc_memory.fields.seconds;
}
uint8_t m41t62_minutes()
{
  uint8_t raw = read_single_register(0x02);
  rtc_memory.fields.minutes = (raw & 0xf);
  rtc_memory.fields.tenminutes = ((raw & 0x70) >> 4);
  return (rtc_memory.fields.tenminutes * 10) + rtc_memory.fields.minutes;
}
uint8_t m41t62_hours()
{
  uint8_t raw = read_single_register(0x03);
  rtc_memory.fields.hours = (raw & 0xf);
  rtc_memory.fields.tenhours = ((raw & 0x30) >> 4);
  return (rtc_memory.fields.tenhours * 10) + rtc_memory.fields.hours;
}
uint8_t m41t62_day_of_the_week()
{
  uint8_t raw = read_single_register(0x05);
  rtc_memory.fields.dayofweek = (raw & 0xf);
  return rtc_memory.fields.dayofweek;
}

uint8_t m41t62_day()
{
  uint8_t raw = read_single_register(0x05);
  rtc_memory.fields.date = (raw & 0xf);
  rtc_memory.fields.tendate = ((raw & 0x30) >> 4);
  return (rtc_memory.fields.tendate * 10) + rtc_memory.fields.date;
}

uint8_t m41t62_month()
{
  uint8_t raw = read_single_register(0x06);
  rtc_memory.fields.month = (raw & 0xf);
  rtc_memory.fields.tenmonth = ((raw & 0x10) >> 4);
  return (rtc_memory.fields.tenmonth * 10) + rtc_memory.fields.month;
}

uint32_t m41t62_year()
{
  uint8_t raw = read_single_register(0x07);
  rtc_memory.fields.year = (raw & 0xf);
  rtc_memory.fields.tenyear = ((raw & 0xf0) >> 4);
  return (rtc_memory.fields.tenyear * 10) + rtc_memory.fields.year;
}

void m41t62_set_date(uint8_t seconds, uint8_t minutes, uint8_t hours,
                     uint8_t day, uint8_t month, uint16_t year)
{
  rtc_memory.fields.tenseconds = seconds / 10;
  rtc_memory.fields.seconds = seconds % 10;

  rtc_memory.fields.tenminutes = minutes / 10;
  rtc_memory.fields.minutes = minutes % 10;

  rtc_memory.fields.tenhours = hours / 10;
  rtc_memory.fields.hours = hours % 10;

  rtc_memory.fields.tendate = day / 10;
  rtc_memory.fields.date = day % 10;

  rtc_memory.fields.tenmonth = month / 10;
  rtc_memory.fields.month = month % 10;

  rtc_memory.fields.tenyear = year / 10;
  rtc_memory.fields.year = year % 10;

  write_whole_memory();
  //NRF_LOG_HEXDUMP_INFO(&rtc_memory, sizeof(rtc_memory));
}

void m41t62_set_alarm_date(uint8_t seconds, uint8_t minutes, uint8_t hours,
                           uint8_t day, uint8_t month, uint16_t year) {}

void m41t62_set_alarm_periode(m41t62_alarm_t period, void (*alarm_cb)(void))
{
  if (period == M41T62_ALARM_NEVER)
  {
    rtc_memory.fields.AFE = 0; // disable alarm
    write_single_register(0xa, rtc_memory.bytes[0xa]);
    //nrf_drv_gpiote_in_event_enable(RTC_INT, false);
    //NRF_LOG_INFO("RTC Alarm disabled");
  }
  else
  {
    rtc_memory.fields.AFE = 1; // enable alarm

    rtc_memory.fields.RPT1 = ((uint8_t)period & 0x1);
    rtc_memory.fields.RPT2 = (((uint8_t)period >> 1) & 0x1);
    rtc_memory.fields.RPT3 = (((uint8_t)period >> 2) & 0x1);
    rtc_memory.fields.RPT4 = (((uint8_t)period >> 3) & 0x1);
    rtc_memory.fields.RPT5 = (((uint8_t)period >> 4) & 0x1);
    write_single_register(0xa, rtc_memory.bytes[0xa]);
    write_single_register(0xb, rtc_memory.bytes[0xb]);
    write_single_register(0xc, rtc_memory.bytes[0xc]);
    write_single_register(0xd, rtc_memory.bytes[0xd]);
    write_single_register(0xe, rtc_memory.bytes[0xe]);
    alarm_callback = alarm_cb;

    //nrf_drv_gpiote_in_event_enable(RTC_INT, true);

    //NRF_LOG_INFO("RTC Alarm set to 0x%x", period);
  }
  read_single_register(0xf); // move out of the way
}

void m41t62_reset_alarm()
{
  rtc_memory.fields.AFE = 0; // disable alarm
  write_single_register(0xa, rtc_memory.bytes[0xa]);
}

void m41t62_set_calibration(uint8_t value, uint8_t sign)
{
  // not implemented
}

uint8_t m41t62_get_watchdog_flag()
{
  uint8_t reg = read_single_register(0xf);
  if (reg & 0x80)
    return 1; // get D7
  return 0;
}
uint8_t m41t62_get_alarm_flag()
{
  uint8_t reg = read_single_register(0xf);
  if (reg & 0x40)
    return 1; // get D6
  return 0;
}
uint8_t m41t62_get_clock_failure_flag()
{
  uint8_t reg = read_single_register(0xf);
  if (reg & 0x4)
    return 1; // get D3
  return 0;
}

uint8_t m41t62_get_stop_flag()
{
  uint8_t reg = read_single_register(0x1);
  if (reg & 0x80)
    return 1; // get D7
  return 0;
}

/*static void m41t62_int_handler(nrfx_gpiote_pin_t pin,
                               nrf_gpiote_polarity_t action)
{
  read_single_register(0xf); // clear alarm bit
  //NRF_LOG_INFO("RTC: Alert!");
  if (alarm_callback)
    alarm_callback();
}
*/
/*static void m41t62_twi_event_handler(nrf_drv_twi_evt_t const *p_event,
                                     void *p_context)
{
  switch (p_event->type)
  {
  case NRF_DRV_TWI_EVT_DONE:
    switch (p_event->xfer_desc.type)
    {
    case NRF_DRV_TWI_XFER_TX:
      twi_tx_done = true;
      break;
    case NRF_DRV_TWI_XFER_TXTX:
      twi_tx_done = true;
      break;
    case NRF_DRV_TWI_XFER_RX:
      twi_rx_done = true;
      break;
    case NRF_DRV_TWI_XFER_TXRX:
      twi_rx_done = true;
      break;
    default:
      break;
    }
    break;
  case NRF_DRV_TWI_EVT_ADDRESS_NACK:
    NRF_LOG_INFO("RTC Event: ADDRESS NACK");
    twi_tx_done = true;
    twi_rx_done = true;
    break;
  case NRF_DRV_TWI_EVT_DATA_NACK:
    NRF_LOG_INFO("RTC Event: DATA NACK");
    twi_tx_done = true;
    twi_rx_done = true;
    break;
  default:
    break;
  }
}
*/
error_code_t m41t62_init(uint8_t online)
{
  /*ret_code_t err_code;

  const nrf_drv_twi_config_t twi_config = {
      .scl = RTC_SCL,
      .sda = RTC_SDA,
      .frequency = NRF_TWI_FREQ_100K,
      .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
  };

  // we have external I2C pullups
  nrf_gpio_cfg_input(RTC_SCL, NRF_GPIO_PIN_NOPULL);
  nrf_gpio_cfg_input(RTC_SDA, NRF_GPIO_PIN_NOPULL);

  err_code =
      nrf_drv_twi_init(&m_twi, &twi_config, m41t62_twi_event_handler, NULL);
  APP_ERROR_CHECK(err_code);

  nrf_drv_twi_enable(&m_twi);

  NRF_LOG_INFO("Download RTC Memory");
  read_whole_memory();

  // set up gpio for interupt signal
  if (!nrf_drv_gpiote_is_init())
  {
    err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);
  }

  nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
  in_config.pull = NRF_GPIO_PIN_PULLUP;
  err_code = nrf_drv_gpiote_in_init(RTC_INT, &in_config, m41t62_int_handler);
  APP_ERROR_CHECK(err_code);

  if (!online)
  {
    rtc_memory.fields.ST = 0;
    rtc_memory.fields.OFIE = 1;
    rtc_memory.fields.OF = 0;
    write_single_register(0x1, rtc_memory.bytes[0x1]);
    write_single_register(0x2, rtc_memory.bytes[0x2]);
    write_single_register(0xf, rtc_memory.bytes[0xf]);
    // do that directly after stopping the clock
    rtc_memory.fields.ST = 0;
    write_single_register(0x1, rtc_memory.bytes[0x1]);
    write_single_register(0xf, rtc_memory.bytes[0xf]);
    read_single_register(0xf);
    m41t62_set_alarm_periode(M41T62_ALARM_NEVER, NULL); // disabeling alarm
    // we have been powered down. all time is off anyway
    m41t62_set_date(0, 0, 0, 0, 0, 0);
  }
  */
  return PM_FAIL;
}
