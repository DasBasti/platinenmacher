/**
 * SchmartWatsch Firmware - Generic RTC Driver
 * 
 * Author: Bastian Neumann 2018
 * 
 */
#ifndef M41T62LC6F_H
#define M41T62LC6F_H

/**
 * Registermap for I2C interface
 * 
 * Addr | D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 | Function               | range BDC format
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  00h |    0.1 seconds    |   0.01 seconds    | 10th /100th of seconds | 00-99
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  01h | ST |  10 seconds  |      seconds      | Seconds                | 00-59 
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  02h |OFIE|  10 minutes  |      minutes      | Minutes                | 00-59 
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  03h | 0  | 0  |10 hours |    hours (24h)    | Hours                  | 00-23 
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  04h | RS3| RS2| RS1| RS0| 0  | Day of week  | Day                    | 1-7 
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  05h | 0  | 0  | 10 date | Date: day of month| Date                   | 01-31
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  06h | CB1| CB0| 0  | 10M|       Month       | Century / Month        | 0-3 / 01-12
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  07h |     10 years      |       Year        | Year                   | 00-99
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  08h | OUT| 0  | S  |       Calibration      | Calibration data       |
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  09h | RB2|BMB4|BMB3|BMB2|BMB1|BMB0| RB1| RB0| Watchdog               | 
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  0Ah | AFE|SQWE| 0  | 10M|    Alarm Month    | Month                  | 01-12 
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  0Bh |RPT4|RPT5| 10 date |    Alarm date     | Date                   | 01-31
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  0Ch |RPT3| 0  | 10 hour |    Alarm hour     | Hour                   | 00-23 
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  0Dh |RPT2|  10 minutes  |  Alarm minutes    | Minutes                | 00-59
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  0Eh |RPT1|  10 seconds  |  Alarm seconds    | Seconds                | 00-59
 * -----+----+----+----+----+----+----+----+----+------------------------------------------
 *  0Fh | WDF| AF | 0  | 0  | 0  | OF | 0  | 0  | Flags                  | read only
 *
 * KEYS
 * 0 = must be set to '0'
 * AF = alarm flag (read only)
 * AFE = alarm enable flag
 * BMB0-BMB4 = watchdog multiplier bits
 * CB0-CB1 = century bits
 * OF = oscillator fail bit
 * OFIE = oszillator fail interrupt enable bit
 * OUT = output level
 * RB0-RB2 = watchdog resolution bits
 * RPT1-RPT5 = alarm repeat mode bits
 * RS0-RS3 = SQW frequency bits
 * S = sign bit
 * SQWE = squarewave enable bit
 * ST = stop bit
 * WDF = watchdog flag bit (read only)
 */

#include <stdint.h>
#include "error.h"

#define M41T62_I2C_ADDR 0x68

error_code_t m41t62_init(uint8_t online);

typedef enum
{
    M41T62_ALARM_NEVER,
    M41T62_ALARM_ONCE_PER_YEAR = 0x00,
    M41T62_ALRAM_ONCE_PER_MONTH = 0x10,
    M41T62_ALARM_ONCE_PER_DAY = 0x18,
    M41T62_ALARM_ONCE_PER_HOUR = 0x1c,
    M41T62_ALARM_ONCE_PER_MINUTE = 0x1e,
    M41T62_ALARM_ONCE_PER_SECOND = 0x1f,
} m41t62_alarm_t;

uint32_t m41t62_milliseconds();
uint8_t m41t62_seconds();
uint8_t m41t62_minutes();
uint8_t m41t62_hours();

uint8_t m41t62_day_of_the_week();
uint8_t m41t62_day();
uint8_t m41t62_month();
uint32_t m41t62_year();

void m41t62_set_alarm_date(uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t day, uint8_t month, uint16_t year);
void m41t62_set_alarm_periode(m41t62_alarm_t period, void (*alarm_cb)(void));
void m41t62_set_calibration(uint8_t value, uint8_t sign);

void m41t62_set_date(uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t day, uint8_t month, uint16_t year);

uint8_t m41t62_get_stop_flag();
uint8_t m41t62_get_watchdog_flag();
uint8_t m41t62_get_alarm_flag();
uint8_t m41t62_get_clock_failure_flag();

#endif // M41T62LC6F_H