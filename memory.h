/*
 * Interface to malloc and other RTOS related functions
 *
 *  Created on: Jan 2, 2021
 *      Author: bastian
 */

#ifndef PLATINENMACHER_MEMORY_H_
#define PLATINENMACHER_MEMORY_H_

#include <stdlib.h>
#include "rtos.h"

#define RTOS_Malloc(size) malloc(size)
#define RTOS_Free(size) free(size)

#define bit_set(data, pos) (data |= (1U << pos))
#define bit_clear(data, pos) (data &= (~(1U << pos)))
#define bit_toggle(data, pos) (data ^= (1U << pos))

#endif /* PLATINENMACHER_MEMORY_H_ */
